package cz.hk.lundegaard.contactus;

import cz.hk.lundegaard.contactus.domain.RequestType;
import cz.hk.lundegaard.contactus.repository.RequestTypeRepository;
import cz.hk.lundegaard.contactus.service.RequestTypeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author : vanya.melnykovych
 * @since : 19.01.2022
 */
@ExtendWith(MockitoExtension.class)
public class RequiredTypesTest {

    @Mock
    private RequestTypeRepository requestTypeRepository;

    @InjectMocks
    private RequestTypeService requestTypeService;

    @Test
    void getAllRequestTypes() {
        when(requestTypeRepository.save(any())).then(returnsFirstArg());
        RequestType requestType = requestTypeRepository.save(new RequestType(1L, "Contract Adjustment", null));
        RequestType requestType1 = requestTypeRepository.save(new RequestType(2L, "Damage Case", null));
        List<RequestType> requestTypes = new ArrayList<>();
        requestTypes.add(requestType);
        requestTypes.add(requestType1);
        when(requestTypeRepository.findAll()).thenReturn(requestTypes);

        List<RequestType> result = requestTypeService.getAllRequestTypes();
        assertEquals(2, result.size());
        verify(requestTypeRepository, times(1)).findAll();
    }
}

package cz.hk.lundegaard.contactus;

import cz.hk.lundegaard.contactus.domain.ContactUs;
import cz.hk.lundegaard.contactus.domain.RequestType;
import cz.hk.lundegaard.contactus.payload.request.ContactUsRequest;
import cz.hk.lundegaard.contactus.payload.response.ResponseMessage;
import cz.hk.lundegaard.contactus.repository.ContactUsRepository;
import cz.hk.lundegaard.contactus.repository.RequestTypeRepository;
import cz.hk.lundegaard.contactus.service.ContactUsService;
import cz.hk.lundegaard.contactus.service.RequestTypeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

/**
 * @author : vanya.melnykovych
 * @since : 19.01.2022
 */
@ExtendWith(MockitoExtension.class)
public class ContactUsTests {

    @InjectMocks
    private ContactUsService contactUsService;

    @Mock
    private ContactUsRepository contactUsRepository;

    @Mock
    private RequestTypeService requestTypeService;

    @Mock
    private RequestTypeRepository requestTypeRepository;

    @Test
    public void saveContactUsRequest() {
        when(requestTypeService.findById(1L)).thenReturn(this.initRequestType());
        when(contactUsRepository.save(any())).thenReturn(initContactUs());
        
        ContactUsRequest contactUsRequest = new ContactUsRequest();
        contactUsRequest.setRequestTypeId(1L);
        contactUsRequest.setEmail("email@email.com");
        contactUsRequest.setPolicyNumber(1597554L);
        contactUsRequest.setName("Name");
        contactUsRequest.setSurname("Surname");
        contactUsRequest.setRequestDescription("Something went wrong!");

        requestTypeRepository.save(this.initRequestType());

        ResponseMessage responseMessage = contactUsService.saveContactUsEntity(contactUsRequest);
        System.out.println(responseMessage);
        verify(requestTypeService, times(1)).findById(1L);
        assertEquals("Contact us form was successfully saved!", responseMessage.getMessage());
    }

    private ContactUs initContactUs() {
        ContactUs contactUs = new ContactUs();
        contactUs.setId(1L);
        contactUs.setRequestType(this.initRequestType());
        contactUs.setEmail("email@email.com");
        contactUs.setPolicyNumber(1597554L);
        contactUs.setName("Name");
        contactUs.setSurname("Surname");
        contactUs.setRequestDescription("Something went wrong!");
        return contactUs;
    }

    private RequestType initRequestType() {
        return new RequestType(1L, "Contract Adjustment", null);
    }
}

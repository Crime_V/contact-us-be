package cz.hk.lundegaard.contactus.annotation;

import cz.hk.lundegaard.contactus.validator.StringValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @author : vanya.melnykovych
 * @since : 19.01.2022
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = StringValidator.class)
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
public @interface ValidString {

    String message() default "String is invalid";

    Class<?>[] groups() default{};

    Class<? extends Payload>[] payload() default {};
}

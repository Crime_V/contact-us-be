package cz.hk.lundegaard.contactus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContactUsBeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ContactUsBeApplication.class, args);
    }

}

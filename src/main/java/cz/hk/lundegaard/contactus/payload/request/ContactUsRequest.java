package cz.hk.lundegaard.contactus.payload.request;

import cz.hk.lundegaard.contactus.annotation.ValidEmail;
import cz.hk.lundegaard.contactus.annotation.ValidString;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author : vanya.melnykovych
 * @since : 19.01.2022
 */
@Data
public class ContactUsRequest {

    @NotNull
    private Long requestTypeId;

    @ValidEmail
    private String email;

    @NotNull(message = "Policy number is mandatory")
    private Long policyNumber;

    @ValidString(message = "Name is empty or contains invalid characters")
    private String name;

    @ValidString(message = "Surname is empty or contains invalid characters")
    private String surname;

    @Size(max = 5000)
    @NotEmpty(message = "Request description cannot be empty")
    private String requestDescription;
}

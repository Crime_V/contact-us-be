package cz.hk.lundegaard.contactus.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author : vanya.melnykovych
 * @since : 19.01.2022
 */
@Data
@AllArgsConstructor
public class ResponseMessage {

    private String message;
}

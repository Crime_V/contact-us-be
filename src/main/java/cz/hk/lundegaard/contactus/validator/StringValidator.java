package cz.hk.lundegaard.contactus.validator;

import cz.hk.lundegaard.contactus.annotation.ValidString;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author : vanya.melnykovych
 * @since : 19.01.2022
 */
@Service
public class StringValidator implements ConstraintValidator<ValidString, String> {

    private static final String STRING_PATTERN = "[a-zA-Z]+";

    @Override
    public void initialize(ValidString constraintAnnotation) {

    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext constraintValidatorContext) {
        return validateString(email);
    }

    private boolean validateString(String str) {
        if (Strings.isEmpty(str)) {
            return false;
        }
        Pattern pattern = Pattern.compile(STRING_PATTERN);
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }
}

package cz.hk.lundegaard.contactus.controller;

import com.fasterxml.jackson.annotation.JsonView;
import cz.hk.lundegaard.contactus.domain.RequestType;
import cz.hk.lundegaard.contactus.domain.util.JsonViewFilters;
import cz.hk.lundegaard.contactus.service.RequestTypeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author : vanya.melnykovych
 * @since : 19.01.2022
 */
@RestController
@RequestMapping("/rest/api/request-types")
public class RequestTypeController {

    private final RequestTypeService requestTypeService;

    public RequestTypeController(RequestTypeService requestTypeService) {
        this.requestTypeService = requestTypeService;
    }

    @GetMapping(value = "/all")
    @JsonView(JsonViewFilters.Basic.class)
    public ResponseEntity<List<RequestType>> getAllRequestTypes() {
        return new ResponseEntity<>(requestTypeService.getAllRequestTypes(), HttpStatus.OK);
    }
}

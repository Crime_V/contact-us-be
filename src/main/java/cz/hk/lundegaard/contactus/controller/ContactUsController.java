package cz.hk.lundegaard.contactus.controller;

import cz.hk.lundegaard.contactus.controller.util.ControllerUtils;
import cz.hk.lundegaard.contactus.payload.request.ContactUsRequest;
import cz.hk.lundegaard.contactus.service.ContactUsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author : vanya.melnykovych
 * @since : 19.01.2022
 */
@RestController
@RequestMapping("/rest/api/contact-us")
public class ContactUsController {

    private final ContactUsService contactUsService;

    public ContactUsController(ContactUsService contactUsService) {
        this.contactUsService = contactUsService;
    }

    @PostMapping(value = "/save")
    public ResponseEntity<Object> saveContactUsEntity(@RequestBody @Valid ContactUsRequest contactUsRequest,
                                                      BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ControllerUtils.getErrorResponse(bindingResult);
        }
        return new ResponseEntity<>(contactUsService.saveContactUsEntity(contactUsRequest), HttpStatus.OK);
    }
}

package cz.hk.lundegaard.contactus.service;

import cz.hk.lundegaard.contactus.domain.RequestType;
import cz.hk.lundegaard.contactus.repository.RequestTypeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author : vanya.melnykovych
 * @since : 19.01.2022
 */
@Service
public class RequestTypeService {

    private final RequestTypeRepository requestTypeRepository;

    public RequestTypeService(RequestTypeRepository requestTypeRepository) {
        this.requestTypeRepository = requestTypeRepository;
    }

    public List<RequestType> getAllRequestTypes() {
        return requestTypeRepository.findAll();
    }

    public RequestType findById(Long id) {
        return requestTypeRepository.getById(id);
    }
}

package cz.hk.lundegaard.contactus.service;

import cz.hk.lundegaard.contactus.domain.ContactUs;
import cz.hk.lundegaard.contactus.domain.RequestType;
import cz.hk.lundegaard.contactus.payload.request.ContactUsRequest;
import cz.hk.lundegaard.contactus.payload.response.ResponseMessage;
import cz.hk.lundegaard.contactus.repository.ContactUsRepository;
import org.springframework.stereotype.Service;

/**
 * @author : vanya.melnykovych
 * @since : 19.01.2022
 */
@Service
public class ContactUsService {

    private final RequestTypeService requestTypeService;
    private final ContactUsRepository contactUsRepository;

    public ContactUsService(RequestTypeService requestTypeService, ContactUsRepository contactUsRepository) {
        this.requestTypeService = requestTypeService;
        this.contactUsRepository = contactUsRepository;
    }

    public ResponseMessage saveContactUsEntity(ContactUsRequest contactUsRequest) {
        ContactUs contactUs = this.mapContactUsRequest(contactUsRequest);
        ContactUs savedEntity = contactUsRepository.save(contactUs);
        if (savedEntity.getId() == null)  {
            return new ResponseMessage("Failed to save contact us request!");
        }
        return new ResponseMessage("Contact us form was successfully saved!");
    }

    private ContactUs mapContactUsRequest(ContactUsRequest contactUsRequest) {
        ContactUs contactUs = new ContactUs();
        contactUs.setRequestType(this.findRequestType(contactUsRequest.getRequestTypeId()));
        contactUs.setEmail(contactUsRequest.getEmail());
        contactUs.setPolicyNumber(contactUsRequest.getPolicyNumber());
        contactUs.setName(contactUsRequest.getName());
        contactUs.setSurname(contactUsRequest.getSurname());
        contactUs.setRequestDescription(contactUsRequest.getRequestDescription());
        return contactUs;
    }

    private RequestType findRequestType(Long requestTypeId) {
        return requestTypeService.findById(requestTypeId);
    }
}

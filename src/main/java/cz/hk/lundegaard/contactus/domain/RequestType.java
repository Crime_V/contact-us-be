package cz.hk.lundegaard.contactus.domain;

import com.fasterxml.jackson.annotation.JsonView;
import cz.hk.lundegaard.contactus.domain.util.JsonViewFilters;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * @author : vanya.melnykovych
 * @since : 19.01.2022
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class RequestType {

    @Id
    @JsonView(JsonViewFilters.Basic.class)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonView(JsonViewFilters.Basic.class)
    private String name;

    @OneToMany(mappedBy = "requestType")
    private List<ContactUs> contactUs;
}

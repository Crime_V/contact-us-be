package cz.hk.lundegaard.contactus.domain;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author : vanya.melnykovych
 * @since : 19.01.2022
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "contact_us")
public class ContactUs {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private RequestType requestType;

    @Column(nullable = false)
    private String email;

    @Column(name = "policy_number", nullable = false)
    private Long policyNumber;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String surname;

    @Column(name = "request_description", nullable = false)
    private String requestDescription;
}

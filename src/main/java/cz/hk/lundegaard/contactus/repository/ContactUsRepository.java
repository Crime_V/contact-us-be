package cz.hk.lundegaard.contactus.repository;

import cz.hk.lundegaard.contactus.domain.ContactUs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author : vanya.melnykovych
 * @since : 19.01.2022
 */
@Repository
public interface ContactUsRepository extends JpaRepository<ContactUs, Long> {
}
